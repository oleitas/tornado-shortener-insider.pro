import unittest
import urllib
import string
from random import random, choice
from funkload.FunkLoadTestCase import FunkLoadTestCase
import json

class Simple(FunkLoadTestCase):
    """This test use a configuration file Simple.conf."""
    def setUp(self):
        """Setting up test."""
        self.shorten_url = self.conf_get('main', 'shorten_url')
        self.expand_url = self.conf_get('main', 'expand_url')

    def get_redirect(self, url):
 
        url_percent = urllib.quote(url, '')
        self.logd(url_percent)
        self.get(self.shorten_url+'?longUrl='+url_percent, description='Get URL')
        bodyjson = json.loads(self.getBody())
        self.logd(self.getBody())
        shortened_url = bodyjson.get('data').get('url')
        b = self.exists(shortened_url)
        a = self.getLastUrl()
        self.assert_(a in url, (a,url,))


    def test_normal_url_redirect(self):
        url = 'https://someotherdomain.com/?param1=value1&param2=param2'
        self.get_redirect(url)


    def test_expand_output(self):
        def generate_string(n):
            return ''.join(choice(string.ascii_uppercase + string.digits) for _ in range(n))
        url = 'http://'+generate_string(5)+'.com/'+'?'+generate_string(3)+'='+generate_string(5)
        url_percent = urllib.quote(url, '')
        nb_time = self.conf_getInt('test_simple', 'nb_time')
        for i in range(nb_time):
            self.get(self.shorten_url+'?longUrl='+url_percent, description='Get URL')
            bodyjson = json.loads(self.getBody())
            self.logd(self.getBody())
            shortened_url = bodyjson.get('data').get('url')
            expand_url = self.expand_url+'?shortUrl='+shortened_url
            self.get(expand_url)
            bodyjson = json.loads(self.getBody())
            long_url = bodyjson.get('data').get('expand')[0].get('long_url')
            self.assert_(long_url == url, 'Urls dont match')


if __name__ in ('main', '__main__'):
    unittest.main()

